module main

go 1.17

require github.com/rs/xid v1.3.0

require (
	github.com/go-kivik/couchdb/v3 v3.2.8 // indirect
	github.com/go-kivik/kivik/v3 v3.2.3 // indirect
	github.com/go-kivik/mango v0.0.3 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	golang.org/x/net v0.0.0-20201110031124-69a78807bb2b // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
)
