package main

import (
	"fmt"
	"log"
	"net/http"

	_ "github.com/go-kivik/couchdb/v3"

	"github.com/gorilla/mux"
)

func main() {

	// ================= start router ===================
	fmt.Println("Starting")
	r := mux.NewRouter()
	sr := r.PathPrefix("/api/v1/students").Subrouter()

	// ================= sr endpoints ===================
	sr.HandleFunc("", FetchAllStudents).Methods("GET")
	sr.HandleFunc("/{id}", FindStudentByStudentId).Methods("GET")
	sr.HandleFunc("/doc_id/{id}", FindStudentByDocId).Methods("GET")
	sr.HandleFunc("/class/{classname}", FindStudentByClassname).Methods("GET")
	sr.HandleFunc("", PutStudent).Methods("PUT")
	sr.HandleFunc("/bulk", BulkInsert).Methods("PUT")
	sr.HandleFunc("", DeleteStudent).Methods("DELETE")

	// ================= serve ===================
	log.Fatal(http.ListenAndServe(":8080", r))
	fmt.Println("Ended process")
}
