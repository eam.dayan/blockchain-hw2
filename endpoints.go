package main

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"

	"github.com/go-kivik/kivik/v3"
	"github.com/gorilla/mux"
	"github.com/rs/xid"
)

func BulkInsert(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	// get records from api
	resp, err := http.Get("https://api.jsonbin.io/v3/b/6166ea0baa02be1d4458d5f6")
	if err != nil {
		log.Fatalln(err)
	}
	// convert response to byte array
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}
	// convert byte array to record object
	var studentRecord *StudentRecord
	if err := json.Unmarshal(body, &studentRecord); err != nil {
		panic(err)
	}

	// get database
	db := getDb()

	// create array to store created
	var insertedStudents []IdJson

	// put every object in the array
	for _, student := range studentRecord.Record {
		id := generateId()
		rev, err := db.Put(context.TODO(), id, student)

		if err != nil {
			panic(err)
		}
		// save response into array
		insertedStudent := IdJson{Id: id, RevisionId: rev}
		insertedStudents = append(insertedStudents, insertedStudent)
	}

	// add array to map
	response := make(map[string][]IdJson)
	response["inserted"] = insertedStudents
	json.NewEncoder(w).Encode(response)
}

func PutStudent(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var id string

	// get request
	var student Student
	_ = json.NewDecoder(r.Body).Decode(&student)

	// get database
	db := getDb()

	if student.Id == "" {
		// generate id for insert if id does not exist
		id = generateId()
	} else {
		id = student.Id
	}

	// if _rev != "", it will update and not create a new doc
	rev, err := db.Put(context.TODO(), id, student)
	if err != nil {
		panic(err)
	}

	// display revision id
	studentResponse := IdJson{Ok: true, Id: id, RevisionId: rev}
	json.NewEncoder(w).Encode(studentResponse)
}

func FindStudentByDocId(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	// get path variable
	id := mux.Vars(r)["id"]

	var req map[string]string
	_ = json.NewDecoder(r.Body).Decode(&req)

	// get from couchdb
	db := getDb()
	var studentResponse Student

	if len(req) != 0 {
		db.Get(context.TODO(), id, kivik.Options{
			"rev": req["rev"],
		}).ScanDoc(&studentResponse)
	} else {
		db.Get(context.TODO(), id).ScanDoc(&studentResponse)
	}

	// return response
	json.NewEncoder(w).Encode(studentResponse)
}

func FindStudentByClassname(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	// get path variable
	classname := mux.Vars(r)["classname"]

	db := getDb()

	// get from couchdb
	var studentResponse []Student
	rows, err := db.Query(context.TODO(), "_design/student", "_view/by_classname", kivik.Options{
		"key": classname,
	})

	// var result []byte
	// selector, err := mango.New("{\"selector\": {\"classname\": {\"$eq\": \"" + classname + "\"}}}")
	// selector.UnmarshalJSON(result)
	// fmt.Println(result)

	// check error
	if err != nil {
		panic(err)
	}

	// read all from results
	for rows.Next() {
		var r Student
		rows.ScanValue(&r)
		studentResponse = append(studentResponse, r)
	}
	// return response
	json.NewEncoder(w).Encode(studentResponse)
}

func FindStudentByStudentId(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	// get path variable
	param := mux.Vars(r)["id"]
	id, err := strconv.Atoi(param)

	if err != nil {
		panic(err)
	}

	db := getDb()
	// get from couchdb
	var studentResponse []Student
	var rows *kivik.Rows
	rows, err = db.Query(context.TODO(), "_design/student", "_view/by_studentid", kivik.Options{
		"key": id,
	})
	if err != nil {
		panic(err)
	}

	// read all from results
	for rows.Next() {
		var r Student
		rows.ScanValue(&r)
		studentResponse = append(studentResponse, r)
	}

	// return response
	json.NewEncoder(w).Encode(studentResponse)
}

func FetchAllStudents(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	db := getDb()
	// get from couchdb
	var studentResponse []Student
	var rows *kivik.Rows
	rows, err := db.Query(context.TODO(), "_design/student", "_view/by_studentid")
	if err != nil {
		panic(err)
	}

	// read all from results
	for rows.Next() {
		var r Student
		rows.ScanValue(&r)
		studentResponse = append(studentResponse, r)
	}

	// return response
	json.NewEncoder(w).Encode(studentResponse)
}

func DeleteStudent(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	// get request
	var deleteRequest IdJson
	_ = json.NewDecoder(r.Body).Decode(&deleteRequest)

	// couchdb delete
	db := getDb()
	newRev, err := db.Delete(context.TODO(), deleteRequest.Id, deleteRequest.RevisionId)
	if err != nil {
		panic(err)
	}

	// return response
	updateResponse := IdJson{Id: deleteRequest.Id, RevisionId: newRev}
	json.NewEncoder(w).Encode(updateResponse)
}

func getDb() *kivik.DB {

	dsn := "http://admin:xcom123@35.198.215.85:5984"

	// get client
	client, err := kivik.New("couch", dsn)
	if err != nil {
		panic(err)
	}

	// get database
	db := client.DB(context.TODO(), "students")
	return db
}

func generateId() string {
	return xid.New().String()
}
