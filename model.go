package main

type Student struct {
	Id             string `json:"_id,omitempty"`
	RevisionId     string `json:"_rev,omitempty"`
	StudentId      int    `json:"studentid"`
	StudentName    string `json:"studentname"`
	Gender         string `json:"gender"`
	Info           string `json:"info"`
	IsClassMonitor bool   `json:"is_class_monitor"`
	ClassName      string `json:"classname"`
	TeamName       string `json:"teamname"`
}

type IdJson struct {
	Ok         bool   `json:"ok,omitempty"`
	Id         string `json:"_id"`
	RevisionId string `json:"_rev"`
}

type StudentRecord struct {
	Record []Student `json:"record"`
}

