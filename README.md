# GO - CouchDB

_Blockchain: Homework2_

_Name: EAM DAYAN_

## Index

1. Insert one
2. Bulk insert
3. Find document
    3. 1 Find document by student id
    3. 2 Find document by doc_id and optional rev
    3. 3 Fetch all
4. Update
5. Delete
6. Filter Students by Classname

## 1. Insert

Insert by using `PUT` and providing the document in json format.

```HTTP
PUT /api/v1/students HTTP/1.1
Host: localhost:8080
Authorization: Basic YWRtaW46eGNvbTEyMw==
Content-Type: application/json
Content-Length: 257

{
    "studentid": 11,
    "studentname": "DAN BROWN",
    "gender": "M",
    "info": null,
    "is_class_monitor": null,
    "classname": "BTB",
    "teamname": "Team 3"
}
```

## 2. Bulk Insert

Simple send request with `PUT` method to `localhost:8080/api/v1/students/bulk` and it will automatically request JSON data from <https://api.jsonbin.io/v3/b/6166ea0baa02be1d4458d5f6> and insert them into the database.

```HTTP
PUT /api/v1/students/bulk HTTP/1.1
Host: localhost:8080
Authorization: Basic YWRtaW46eGNvbTEyMw==
```

## 3. Find document

### 3.1 Find Document by Student ID

Get all students by simply sending a `GET` request to `http://localhost:8080/api/v1/students/{studentid}`

```HTTP
GET /api/v1/students/22 HTTP/1.1
Host: localhost:8080
Authorization: Basic YWRtaW46eGNvbTEyMw==
```

### 3.2 Find Document by Doc Id and Revision Id

Get all students by simply sending a `GET` request to `http://localhost:8080/api/v1/students/{_id}` and an optional revision number `_rev` in the request body.

```HTTP
GET /api/v1/students/doc_id/c5kjehjrbtkrdt08lhd0 HTTP/1.1
Host: localhost:8080
Authorization: Basic YWRtaW46eGNvbTEyMw==
Content-Type: application/json
Content-Length: 51

{
    "rev": "2-ab693338ed7c5224bfa77714e9904517"
}
```

### 3.3 Fetch All

Get all students by simply sending a `GET` request to `http://localhost:8080/api/v1/students`

``` HTTP
GET /api/v1/students HTTP/1.1
Host: localhost:8080

```

## 4. Update

Send a `PUT` request to `http://localhost:8080/api/v1/students` and be sure to provide `_rev` for the update to work. Otherwise, it would insert the body as a new document instead.

``` HTTP
PUT /api/v1/students HTTP/1.1
Host: localhost:8080
Authorization: Basic YWRtaW46eGNvbTEyMw==
Content-Type: application/json
Content-Length: 257

{
    "_id": "c5kjehjrbtkrdt08lhd0",
    "_rev": "1-cf089669c666d95d5318839a19bacb82",
    "studentid": 11,
    "studentname": "EAM DAYAN",
    "gender": "M",
    "info": null,
    "is_class_monitor": null,
    "classname": "BTB",
    "teamname": "Team 3"
}
```

## 5. Delete

Send a `DELETE` request to `http://localhost:8080/api/v1/students` while providing `_id` and `_rev` in the request body.

``` HTTP
DELETE /api/v1/students/ HTTP/1.1
Host: localhost:8080
Authorization: Basic YWRtaW46eGNvbTEyMw==
Content-Type: application/json
Content-Length: 87

{
    "_id": "c5kfpajrbtknenn7dllg",
    "_rev": "2-5bbfa372befdb3e8ea52fadc0e2cbfad"
}
```

## 6. Filter Students by Classname

Send a `GET` request to `http://localhost:8080/api/v1/students/class/{class}` where class is the name of the class you want to filter by.

``` HTTP
GET /api/v1/students/class/BTB HTTP/1.1
Host: localhost:8080
```

## 7. Why do need to have a REST API for an existing API of CouchDB?

CouchDB already provides an automatically generated REST API for your databases but they may not be enough or what you want or they may not work exactly the way you want it. Creating another layer of API on top of the existing API will give you that extra layer of customization.

We may also want to conform with the 3 tier application architecture which consists of a presentation layer, a logic layer and a data layer. Our Go REST API will act as the logic layer and provided more complicated methods and will give you the option of adding extra security features to your RESTful application.
